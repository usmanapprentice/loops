function HextoDex (n) {
	let hex = n[0], result = 0, digitValue;
	hex = hex.toLowerCase();
	for(i = 0; i < hex.length ; i += 1){
		digitValue = '0123456789abcdef'.indexOf(hex[i]);
		result = result * 16 + digitValue;
	}
	return result;
}
console.log(HextoDex(['FE']));