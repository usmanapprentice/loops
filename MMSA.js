function MMSA (array) {
	let min, max, sum = 0, avg;
	let len = array.length;
	array = array.map(x=>+x);
	min = array[0];
	max = array[0];
	for(i=0;i<len;i++){
		sum  += array[i];
		if (min>array[i]) {
			min = array[i];
		}
		if (max < array[i]) {
			max = array[i];
		}
	}
	avg = ((sum==0)?0:sum/len).toFixed(2);
	min = min.toFixed(2);
	max = max.toFixed(2);
	sum = sum.toFixed(2);
	console.log(`Min = ${min}`);
	console.log(`Max = ${max}`);
	console.log(`Sum = ${sum}`);
	console.log(`Avg = ${avg}`);
}

MMSA(['2', '5', '1']
);